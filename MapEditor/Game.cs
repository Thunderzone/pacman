using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Net;
using Microsoft.Xna.Framework.Storage;


using System.Xml;

using Pacman;

namespace MapEditor
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class MapEditor : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        #region Attributes

        private Field[,] fields = new Field[25,25];

        private Sprite[,] grids = new Sprite[25,25];

        Texture2D gridTexture;
        Texture2D selectionTexture;
        Texture2D toolTexture;
        Texture2D toolSelectionTexture;

        Sprite selection;
        Sprite toolSelection;

        bool isPacman = false;
        Point pacmanSpawn;
        List<Point> ghostSpawn = new List<Point> { };
        List<Point> doors = new List<Point> { };
        List<Point> powerups = new List<Point> { };

        static int toolNumber = 15;
        int toolIndex = 0;

        private Rectangle[] toolRectangles = new Rectangle[toolNumber];
        private Sprite[] toolSprites = new Sprite[toolNumber];

        Field selectedField = null;

        bool showGrid = true;

        KeyboardState keyboardLastState;
        MouseState mouseLastState;

        SpriteFont font;

        Point cursorPosition;
        bool isCursorOnMap = false;

        bool flip = false;

        #endregion

        public MapEditor()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 550;
            graphics.PreferredBackBufferHeight = 550;

        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            System.Windows.Forms.Cursor.Show();

            keyboardLastState = Keyboard.GetState();
            mouseLastState = Mouse.GetState();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            gridTexture = Content.Load<Texture2D>(@"GridTex");
            selectionTexture = Content.Load<Texture2D>(@"SelectionTex");
            toolTexture = Content.Load<Texture2D>(@"tools");
            toolSelectionTexture = Content.Load<Texture2D>(@"ToolSelTex");

            font = Content.Load<SpriteFont>("font");
            font.Spacing = 10.0f;

            for( int i = 0; i < fields.GetLength(0); i++)
            {
                for (int j = 0; j < fields.GetLength(1); j++)
                {
                    Vector2 position = new Vector2(20.0f * i, 20.0f * j);

                    /// 
                    Sprite grid = new Sprite(position, gridTexture, 
                                                new Rectangle(0, 0, 20, 20), 
                                                Vector2.Zero );

                    /// Ubacuje sprajtove za grid u matricu
                    grids[i, j] = grid;

                    /// Stvara nova polja i ubacuje ih u matricu
                    fields[i, j] = new Field(i, j, toolTexture, 0, false, Field.SpriteOrientation.North);
                }
            }

            int toolSpritePositionY = 30;

            for (int i = 0; i < toolNumber; i++ )
            {
                toolSprites[i] = new Sprite(new Vector2(515.0f, (float)toolSpritePositionY),
                                            toolTexture, new Rectangle(i * 20, 0, 20, 20), Vector2.Zero);
                toolRectangles[i] = new Rectangle(515, toolSpritePositionY, 20, 20);

                toolSpritePositionY += 30;
            }

            toolSelection = new Sprite(new Vector2(514.0f, 29.0f), toolSelectionTexture,
                                        new Rectangle(0, 0, 22, 22), Vector2.Zero);

            selection = null;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            // TODO: Add your update logic here
            KeyboardState keyboardSate = Keyboard.GetState();
            MouseState mouseState = Mouse.GetState();

            if (mouseState.X >= 0 && mouseState.X < 500 &&
                mouseState.Y >= 0 && mouseState.Y < 500)
            {
                cursorPosition.X = mouseState.X / 20;
                cursorPosition.Y = mouseState.Y / 20;

                isCursorOnMap = true;
            }
            else isCursorOnMap = false;


            if ( mouseState.LeftButton == ButtonState.Pressed)
            {
                #region Menjanje polja na mapi
	            if (isCursorOnMap)
	            {
	                int i = cursorPosition.X;
	                int j = cursorPosition.Y;
	

	                selection = new Sprite(new Vector2(i * 20.0f, j * 20.0f), selectionTexture,
	                                        new Rectangle(0, 0, 20, 20), Vector2.Zero);

                    bool isWall = (toolIndex >=1 && toolIndex < 11) ? true : false;
                        
                    if (selectedField != fields[i, j])
                    {
                        if (!isWall)
                        {
                            if (toolIndex == 11)
                            {
                                fields[pacmanSpawn.X, pacmanSpawn.Y] = new Field(pacmanSpawn.X, pacmanSpawn.Y, toolTexture, 0, false, Field.SpriteOrientation.North);
                                pacmanSpawn = new Point(i, j);
                            }

                            if (toolIndex == 12) doors.Add(new Point(i, j));
                            if (toolIndex == 13) powerups.Add(new Point(i, j));
                            if (toolIndex == 14) ghostSpawn.Add(new Point(i, j));
                        }
                    }

                    fields[i, j] = new Field(i, j, toolTexture, toolIndex, isWall, Field.SpriteOrientation.North);
                    selectedField = fields[i, j];  

                    if (flip && isWall)
                    {
                        int flipI = 24 - i;
                        fields[flipI, j] =  new Field(flipI, j, toolTexture, toolIndex, isWall, Field.SpriteOrientation.North);
                        fields[flipI, j].RotateLeft();
                        fields[flipI, j].RotateLeft();
                    }
         
                }
                #endregion

                #region Selektovanje tool-a

                for (int i = 0; i < toolNumber; i++)
                {
                    bool isInterect = false;
                    Rectangle rec = new Rectangle(mouseState.X, mouseState.Y, 1, 1);

                    toolRectangles[i].Intersects(ref rec, out isInterect);

                    if (isInterect)
                    {
                        toolIndex = i;
                        toolSelection = new Sprite(new Vector2(514.0f, 29.0f + i * 30.0f),
                                                   toolSelectionTexture, new Rectangle(0, 0, 22, 22),
                                                   Vector2.Zero);
                    }
                }
                #endregion
            }

            if (keyboardLastState.IsKeyUp(Keys.Left) && keyboardSate.IsKeyDown(Keys.Left))
            {
                selectedField.RotateLeft();
                if (flip)
                {
                    fields[24 - selectedField.Position.X, selectedField.Position.Y].RotateRight();
                }
            }
            if (keyboardLastState.IsKeyUp(Keys.Right) && keyboardSate.IsKeyDown(Keys.Right))
            {
                selectedField.RotateRight();
                if (flip)
                {
                    fields[24 - selectedField.Position.X, selectedField.Position.Y].RotateLeft();
                }
            }

            if (keyboardLastState.IsKeyUp(Keys.G) && keyboardSate.IsKeyDown(Keys.G)) 
            {
                showGrid = !showGrid;
            }

            if (keyboardLastState.IsKeyUp(Keys.S) && keyboardSate.IsKeyDown(Keys.S))
            {
                Save();
            }

            if (keyboardLastState.IsKeyUp(Keys.F) && keyboardSate.IsKeyDown(Keys.F)) 
            {
                flip = !flip;
            }


            keyboardLastState = keyboardSate;
            mouseLastState = mouseState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            foreach (Field field in fields)
            {
                field.Draw(spriteBatch);
            }
            if (showGrid)
            {
                foreach (Sprite sprite in grids)
                {
                    sprite.Draw(spriteBatch);
                }
            }

            if(selection != null) selection.Draw(spriteBatch);


            foreach (Sprite sprite in toolSprites)
            {
                sprite.Draw(spriteBatch);
            }


            toolSelection.Draw(spriteBatch);

            #region Crtanje teksta pozicije kursora na ekranu

            Vector2 startPosition = new Vector2(25.0f, 520.0f);

            String ln = @"Ln : ";
            spriteBatch.DrawString(font, ln, startPosition , Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);

            String y = cursorPosition.Y.ToString();
            startPosition += new Vector2(font.MeasureString(ln).X, 0.0f);

            spriteBatch.DrawString(font, y, startPosition, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);


            startPosition = new Vector2(150.0f , 520.0f);
            String col = @"Col : ";
            spriteBatch.DrawString(font, col, startPosition, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);

            String x = cursorPosition.X.ToString();
            startPosition += new Vector2(font.MeasureString(col).X, 0.0f);

            spriteBatch.DrawString(font, x, startPosition, Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);

            String flipYesNo;
            if (flip) flipYesNo = "Flip = Yes";
            else flipYesNo = "Flip = No";
            spriteBatch.DrawString(font, flipYesNo, new Vector2(300.0f, 520.0f), Color.White, 0.0f, Vector2.Zero, 1.0f, SpriteEffects.None, 1.0f);


            #endregion

            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void Save()
        {
            XmlDocument doc = new XmlDocument();

            XmlElement map = doc.CreateElement("map");
            doc.AppendChild(map);

            XmlElement fieldsXml = doc.CreateElement("fields");
            map.AppendChild(fieldsXml);

            foreach (Field field in fields)
            {
                field.WriteToXml(fieldsXml);
            }

            XmlElement pacman = doc.CreateElement("pacman");
            map.AppendChild(pacman);

            pacman.SetAttribute("X", pacmanSpawn.X.ToString());
            pacman.SetAttribute("Y", pacmanSpawn.Y.ToString());

            XmlElement spawns = doc.CreateElement("spawns");
            map.AppendChild(spawns);

            foreach (Point point in ghostSpawn)
            {
                XmlElement spawn = doc.CreateElement("spawn");
                spawns.AppendChild(spawn);
                spawn.SetAttribute("X", point.X.ToString());
                spawn.SetAttribute("Y", point.Y.ToString());
            }

            XmlElement doorsXml = doc.CreateElement("doors");
            map.AppendChild(doorsXml);
            foreach (Point point in doors)
            {
                XmlElement door = doc.CreateElement("door");
                doorsXml.AppendChild(door);
                door.SetAttribute("X", point.X.ToString());
                door.SetAttribute("Y", point.Y.ToString());
            }

            XmlElement powerUpsXml = doc.CreateElement("powerups");
            map.AppendChild(powerUpsXml);
            foreach (Point point in powerups)
            {
                XmlElement powerUpXml = doc.CreateElement("powerup");
                powerUpsXml.AppendChild(powerUpXml);
                powerUpXml.SetAttribute("X", point.X.ToString());
                powerUpXml.SetAttribute("Y", point.Y.ToString());
            }

            doc.Save("map.xml");


        }
    }
}
