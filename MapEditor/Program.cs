using System;

namespace MapEditor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (MapEditor game = new MapEditor())
            {
                game.Run();
            }
        }
    }
}

