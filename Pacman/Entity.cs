﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pacman
{
    public interface IEntity
    {
        void Intersect(Pacman _pacman);
    }
}
