﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno

namespace Pacman
{
    public class Dot : IEntity
    {
        private Sprite sprite;

        public void Intersect(Pacman _pacman)
        {
            
        }

        public Dot(Sprite _sprite)
        {
            sprite = _sprite;
        }

        public void Draw(SpriteBatch _spriteBatch)
        {
            sprite.Draw(_spriteBatch);
        }
    }
}
