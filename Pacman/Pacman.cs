﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno

namespace Pacman
{
    public class Pacman : MoveAnimationListener
    {
        public enum InputDirection
        {
            North=0, West, South, East, Center
        }

        private InputDirection direction;
        private InputDirection requestedDirection;

        private Sprite sprite;

        private MoveAnimation moveAnimation;

        private Vector2 directionVector;

        private int currentAnimationPixelPosition;

        private Vector2 position;

        private int fieldWith = 20;

        private Point currentField;

        private Vector2 initPosition;

        private bool isEndAnimation;

        public void EndAnimation()
        {
            initPosition = initPosition + directionVector * fieldWith;
             



            if (requestedDirection != direction)
            {
                Point fieldPositionReq = GetNeighborPoint(requestedDirection);
                Field fieldReq = GameBoard.GetField(fieldPositionReq);

//                 directionVector = GetDirectionVector(requestedDirection);
//                 direction = requestedDirection;

                if (!fieldReq.IsWall)
                {
                    directionVector = GetDirectionVector(requestedDirection);
                    direction = requestedDirection;
                    sprite.Rotation = -((int)direction + 1) * MathHelper.Pi / 2;
                }
            }

            Point fieldPosition = GetNeighborPoint(direction);
            Field field = GameBoard.GetField(fieldPosition);

            if (field.IsWall)
            {
                requestedDirection = InputDirection.Center;
                direction = InputDirection.Center;
            }


            isEndAnimation = true;
        }

        Vector2 GetDirectionVector(InputDirection _inputDirection)
        {
            Vector2 directionVector = new Vector2();
            switch (_inputDirection)
            {
                case InputDirection.Center:
            	    directionVector = Vector2.Zero;
                    break;
                case InputDirection.East:
                    directionVector = Vector2.UnitX;
                    break;
                case InputDirection.North:
                    directionVector = -Vector2.UnitY;
                    break;
                case InputDirection.South:
                    directionVector = Vector2.UnitY;
                    break;
                case InputDirection.West:
                    directionVector = -Vector2.UnitX;
                    break;
            }
            return directionVector;
        }

        public Pacman(Sprite _sprite, Point _startField)
        {
            moveAnimation = new MoveAnimation(this, 10, fieldWith);
            sprite = _sprite;

            sprite.AddFrame(new Rectangle(20, 0, 20, 20));
            sprite.AddFrame(new Rectangle(40, 0, 20, 20));
            sprite.AddFrame(new Rectangle(60, 0, 20, 20));
            sprite.AddFrame(new Rectangle(80, 0, 20, 20));

            currentField = _startField;

            directionVector = Vector2.UnitX;
            direction = InputDirection.East;
            requestedDirection = InputDirection.East;

            currentAnimationPixelPosition = 0;

            initPosition = new Vector2(fieldWith * _startField.X, fieldWith * _startField.Y);

            sprite.Location = position;
        }

        public void Update(GameTime _gameTime)
        {

            if (direction != InputDirection.Center)
            {
                isEndAnimation = false;
                moveAnimation.Update(_gameTime);

                position = initPosition + directionVector * moveAnimation.PexelPosition;

                sprite.Location = position;
            }


            if(direction == InputDirection.Center)
            {
                if (requestedDirection != InputDirection.Center)
                {
                    Point fieldPosition = GetNeighborPoint(requestedDirection);
                    Field field = GameBoard.GetField(fieldPosition);

                    directionVector = GetDirectionVector(requestedDirection);
                    direction = requestedDirection;
                    sprite.Rotation = -((int)direction + 1) * MathHelper.Pi / 2;

                    if (field.IsWall)
                    {
                        requestedDirection = InputDirection.Center;
                        direction = InputDirection.Center;
                    }
                }
            }
            Point p = GetNeighborPoint(InputDirection.Center);
            Field currentField = GameBoard.GetField(p);

            if (currentField.HasDot())
            {
                currentField.Dot.Intersect(this);
                currentField.Dot = null;
            }
            
            sprite.Update(_gameTime);
   
        }

        private Point GetNeighborPoint(InputDirection _direction)
        {
            currentField.X = (int)sprite.Center.X / 20;
            currentField.Y = (int)sprite.Center.Y / 20;

            switch (_direction)
            {
                case InputDirection.East:
                    return new Point(currentField.X + 1, currentField.Y);
                case InputDirection.North:
                    return new Point(currentField.X, currentField.Y - 1);
                case InputDirection.South:
                    return new Point(currentField.X, currentField.Y + 1);
                case InputDirection.West:
                    return new Point(currentField.X - 1, currentField.Y);
            }
            return currentField;
        }

        public void Draw(SpriteBatch _spriteBatch)
        {
            sprite.Draw(_spriteBatch);
        }

        public InputDirection RequestedDirection
        {
            set { requestedDirection = value; }
        }
    }
}
