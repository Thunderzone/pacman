﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno
using System.Xml;
using System.IO;

namespace Pacman
{
    static class GameBoard
    {
        private static Field[,] fields = new Field[25, 25];

        public static Field GetField(Point _point)
        {
            return fields[_point.X, _point.Y];
        }

        public static Field[,] Fields
        {
            set { fields = value; }
            get { return fields; }
        }

        public static void Draw(SpriteBatch _spriteBatch)
        {
            foreach (Field field in fields)
            {
                field.Draw(_spriteBatch);
            }
        }
    }
}
