﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno
using System.Xml;
using System.IO;

namespace Pacman
{
    static class MapLoader
    {
        private static Point pacmanPosition;

        public static Point PacmanPosition
        {
            get { return pacmanPosition; }
        }

        public static Field[,] Load(String _fileName, Game _game)
        {
            Field[,] fields = new Field[25,25];

            TextWriter tw = new StreamWriter("izlaz.txt");

            XmlDocument doc = new XmlDocument();
            doc.Load("map.xml");

            XmlNodeList fieldsNode = doc.SelectNodes(@"map/fields/field");


            //XmlNodeList fields = map.GetElementsByTagName("fields");

            foreach (XmlNode node in fieldsNode)
            {
                XmlElement element = (XmlElement)node;
                int x = int.Parse(element.GetAttribute("X"));
                int y = int.Parse(element.GetAttribute("Y"));

                bool isWall = bool.Parse(element.GetAttribute("wall"));

                int sheet = int.Parse(element.GetAttribute("sheet"));

                Field.SpriteOrientation orientation = (Field.SpriteOrientation)(int.Parse(element.GetAttribute("orientation")));

                Texture2D texture = _game.Content.Load<Texture2D>(@"walls");
                Texture2D dotTexture = _game.Content.Load<Texture2D>(@"point");
                fields[x, y] = new Field(x, y, texture, sheet, isWall, orientation);

                if (!isWall)
                {
                    Sprite sprite = new Sprite(new Vector2(x*20.0f, y*20.0f), dotTexture, new Rectangle(0,0,20,20), Vector2.Zero);
                    fields[x, y].Dot = new Dot(sprite);
                }
            }

            XmlElement pacmanNode = (XmlElement)doc.SelectSingleNode(@"map/pacman");

            pacmanPosition = new Point(int.Parse(pacmanNode.GetAttribute("X") ), int.Parse(pacmanNode.GetAttribute("Y")));


            tw.Close();

            return fields;
        }
    }
}
