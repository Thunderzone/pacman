﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno
using System.Xml;

namespace Pacman
{
    public class Field
    {
        public enum SpriteOrientation
        {
            North = 0, West, South, East
        }        
        
        private Sprite backgruond;

        private bool isWall;

        private int sheetIndex;

        private Point position;

        private SpriteOrientation orientation;

        private Dot dot;

        public bool IsWall
        {
            get { return isWall; }
        }

        public Microsoft.Xna.Framework.Point Position
        {
            get { return position; }
        }

        public Field(int _row, int _column, Texture2D _texture, int _sheetIndex, bool _isWall, SpriteOrientation _orientation)
        {
            isWall = _isWall;
            position = new Point(_row, _column);

            sheetIndex = _sheetIndex;
            backgruond = new Sprite(new Vector2(_row * 20.0f, _column * 20.0f),
                                    _texture, new Rectangle(_sheetIndex * 20, 0, 20, 20), Vector2.Zero);

            orientation = _orientation;
            backgruond.Rotation = (int)_orientation * MathHelper.Pi / 2.0f;
        }

        public Field(Sprite _background)
        {
            backgruond = _background;
            isWall = false;
            sheetIndex = 0;
        }

        public SpriteOrientation Orientation
        {
            get
            {
                return orientation;
            }
            set 
            {
                orientation = value;
                backgruond.Rotation = (int)value * MathHelper.Pi / 2.0f; 
            }
        }

        public bool HasDot()
        {
            if (dot == null) return false;
            return true;
        }

        public Dot Dot
        {
            get { return dot; }
            set { dot = value; }
        }

        public void RotateRight()
        {
            switch(orientation)
            {
                case SpriteOrientation.North:
                    Orientation = SpriteOrientation.West;
                    break;
                case SpriteOrientation.West:
                    Orientation = SpriteOrientation.South;
                    break;
                case SpriteOrientation.South:
                    Orientation = SpriteOrientation.East;
                    break;
                case SpriteOrientation.East:
                    Orientation = SpriteOrientation.North;
                    break;
            }
        }

        public void RotateLeft()
        {
            switch(orientation)
            {
                case SpriteOrientation.North:
                    Orientation = SpriteOrientation.East;
                    break;
                case SpriteOrientation.West:
                    Orientation = SpriteOrientation.North;
                    break;
                case SpriteOrientation.South:
                    Orientation = SpriteOrientation.West;
                    break;
                case SpriteOrientation.East:
                    Orientation = SpriteOrientation.South;
                    break;
            }
        }

        public void Draw(SpriteBatch _spriteBatch)
        {
            backgruond.Draw(_spriteBatch);
            if (dot != null) dot.Draw(_spriteBatch);
        }

        public void WriteToXml(XmlElement _elem)
        {
            XmlElement field = _elem.OwnerDocument.CreateElement("field");
            field.SetAttribute("X", position.X.ToString());
            field.SetAttribute("Y", position.Y.ToString());
            field.SetAttribute("wall", isWall.ToString());
            if (IsWall)
            {
                field.SetAttribute("sheet", sheetIndex.ToString());
            }
            else
                field.SetAttribute("sheet", "0");
            field.SetAttribute("orientation", ((int)orientation).ToString());

            _elem.AppendChild(field);
        }

    }
}
