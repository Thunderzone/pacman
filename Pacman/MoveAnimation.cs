﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework; // mora da se doda ručno
using Microsoft.Xna.Framework.Graphics; // mora da se doda ručno

namespace Pacman
{
    public class MoveAnimation
    {
        private int pixelPosition = 0;

        private int maxPixels;

        private int transitionPixelTime;

        private int time = 0;

        private MoveAnimationListener listener;

        public void Update(GameTime _gameTime)
        {
            time += _gameTime.ElapsedGameTime.Milliseconds;

            pixelPosition = time / transitionPixelTime;

            if (pixelPosition >= maxPixels)
            {
                time = 0;
                pixelPosition = 0;
                listener.EndAnimation();
            }
        }

        public MoveAnimation(MoveAnimationListener _listener, int _transitionPixelTime, int _maxPixels)
        {
            listener = _listener;
            transitionPixelTime = _transitionPixelTime;
            maxPixels = _maxPixels;
        }

        public int PexelPosition
        {
            get { return pixelPosition; }
        }
    }
}
