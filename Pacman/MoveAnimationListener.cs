﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pacman
{
    public interface MoveAnimationListener
    {
        void EndAnimation();
    }
}
